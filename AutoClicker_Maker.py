#Code to make an autohotkey autoclicker

#Main Function contating everything else
def Main():
	#Function getting the title of the autoclicker
	def TitleGetter():
		#Title of the AutoClicker Being made
		AutoClickerTitle = input("What do you want the file to be named?")
		#Sends the Title to the requested area
		return AutoClickerTitle


	#Function assessing what is needed
	def ClickerSpots():
		#List containing all the clicky spots
		ClickySpotsList = []

		#Tells user how this works
		print("This will ask you how many spots you want clicked and then will continue to ask you for x and y coords, it will do each 1 at a time, dont give more than 1 coordinate per question")
		#Gets the number of clicky spots the user wants
		NumberOfClickySpots = input("How many places do you want to click on?")
		#A loop that loops for each of the clicky spots needed
		for i in range(0,int(NumberOfClickySpots)):
			#Gets the XValue for the spot
			XValue = input("What is the X value for this spot?")
			#Gets the YValue for the spot
			YValue = input("What is the Y value for this spot?")

			#Adds the clicky spots to the list
			ClickySpotsList.append("click " + XValue + ", " + YValue + "\n")
		#Sends the list to the requested area
		return ClickySpotsList


	#Asks what areas are wanted for pixel color searches
	def PixelColorSearchAreas():
		#List containing all the color search areas
		ColorSearchList = []
		#List Containing all the true/false sets
		TrueorFalseValueList = []

		#Gets the number of pixel color areas the user wants
		NumberOfSearchSpots = input("How many areas do you want the color searched for?")
		#A loop that loops for each search spot needed
		for i in range(0,int(NumberOfSearchSpots)):
			#Gets the X1 value
			X1Value = input("What is the X1 value for the area?")
			#Gets the Y1 value
			Y1Value = input("What is the Y1 value for this area?")
			#Gets the X2 value
			X2Value = input("What is the X2 value for this area?")
			#Gets the Y2 value
			Y2Value = input("What is the Y2 value for this area?")
			#Gets the color the user wants
			ColorValue = input("What is the color you want to use based on the results of the color search tool?")
			#Gets the allowed discreptency in the color
			DiscreptencyValue = input("How much discreptency in the color are you ok with? (0 for none)")
			#Asks if the value should activate a click or skip
			TrueorFalseValue = input("Do you want this color to activate a click or skip? (0 for no, 1 for yes (only use 1 as the last one if you do))")
			#Adds the values to the list
			ColorSearchList.append("PixelSearch, xx, yy, " + X1Value + ", " + Y1Value + ", " + X2Value + ", " + Y2Value + ", " + ColorValue + ", " + DiscreptencyValue + ", Fast")
			#Adds the values to the list
			TrueorFalseValueList.append(TrueorFalseValue)
		#If to see if this was used
		if NumberOfSearchSpots == "0":
			#returns nothing
			return None
		#Else to send the data of the list if it was used
		else:
			#Sends the lists to the requested area
			return ColorSearchList + TrueorFalseValueList

	#Function that puts everything together to make the script
	def ScriptMaker(Title, ClickySpots, ColorSearch):
		#Gets first half of the autoclicker script
		#Opens the file
		FirstHalfScript = open("prerequisites/First_Half.txt")
		#Writes this first half into a variable
		FirstHalf = FirstHalfScript.read() + "\n"
		#Closes the file
		FirstHalfScript.close()

		#Gets the second half of the autoclicker script
		#Opens the file
		SecondHalfScript = open("prerequisites/Second_Half.txt")
		#Writes this Second half into a variable
		SecondHalf = SecondHalfScript.read()
		#Closes the file
		SecondHalfScript.close()

		#Variables for ColorSearch
		FalsePixelSearch = ""
		TruePixelSearch1 = ""
		TruePixelSearch2 = ""
		#Checks if colorSearchAreas has anything in the list
		if ColorSearch != None:
			#Gets the color search areas part of the script
			#Opens the file for a false pixel search
			FalsePixelSearchScript = open("prerequisites/PixelSearch_False.txt")
			#Writes the pixel search script into a variable
			FalsePixelSearch = FalsePixelSearchScript.read()
			#Closes the file
			FalsePixelSearchScript.close()

			#Gets the color search areas part of the script
			#Opens the first file for a true pixel search
			TruePixelSearchScript1 = open("prerequisites/PixelSearch_True1.txt")
			#Writes the pixel search script into a variable
			TruePixelSearch1 = TruePixelSearchScript1.read()
			#Closes the file
			TruePixelSearchScript1.close()

			#Gets the color search areas part of the script
			#Opens the second file for a true pixel search
			TruePixelSearchScript2 = open("prerequisites/PixelSearch_True2.txt")
			#Writes the pixel search script into a variable
			TruePixelSearch2 = TruePixelSearchScript2.read()
			#Closes the file
			TruePixelSearchScript2.close()
		else:
			#Passes through else statement
			pass

		#Variable that will take all of the list
		ClickySpotsStr = ""
		#Loop that will serve to turn the list into a single variable
		for i in range(0,len(ClickySpots)):
			#Command that adds each part of the list to the variable
			ClickySpotsStr = ClickySpotsStr + ClickySpots[i]

		#lists that ColorSearch will be split into
		ColorSearchArea = []
		ColorSearchType = []
		#Splits ColorSearch into area and type
		ColorSearchAreas = ColorSearch[:len(ColorSearch)//2]
		ColorSearchType = ColorSearch[len(ColorSearch)//2:]

		#Creates the file for the autoclicker
		AutoClicker = open(Title+".ahk", "w+")
		#Writes The first half of the autoclicker script
		AutoClicker.write(FirstHalf)
		#Closes the file
		AutoClicker.close()
		#Opens the autoclicker file
		AutoClicker = open(Title+".ahk", "a")
		#If to see what should be written
		if ColorSearchAreas == None:
			#Writes the clicky spots to the autoclicker
			AutoClicker.write(ClickySpotsStr)
		#Else is the fallback if there are color search areas
		else:
			#Loop for writing all the color search areas
			for i in range(0,len(ColorSearchType)):
				#If to check if a color search area is looking for a true or false
				if ColorSearchType[i] == "0":
					#Writes the false search area
					AutoClicker.write(ColorSearchAreas[i] + "\n" + FalsePixelSearch + "\n")
				#Fall back if it is a true seach area
				else:
					#Writes the true search area
					AutoClicker.write(ColorSearchAreas[i] + "\n" + TruePixelSearch1 + "\n" + ClickySpotsStr + TruePixelSearch2 + "\n" + SecondHalf)
			#checks if this part is needed
			if ColorSearchType[len(ColorSearchType)-1] == "0":
				#Writes the clicky spots
				AutoClicker.write(ClickySpotsStr + SecondHalf)
			else:
				#passes as there is nothing to add
				pass
		#Closes the file
		AutoClicker.close()

	#Calls ScriptMaker function
	ScriptMaker(TitleGetter(), ClickerSpots(), PixelColorSearchAreas())

Main()